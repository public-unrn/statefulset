# Resumen
https://dev.to/leandronsp/kubernetes-101-part-v-statefulsets-5dob
https://docs.google.com/document/d/1Pk1RdcRmt5Mn6wqrcT0IZOShJv0HERJATa5nkpz-S9I/edit#heading=h.99slju4vt9i6


# Postgres Deployment
https://www.airplane.dev/blog/deploy-postgres-on-kubernetes


## testing
kubectl exec -it [pod-name] --  psql -h localhost -U admin --password -p 5432 postgresdb



# Deployment PostgreSQL 

```
kubectl apply -f ./deployment
```

# StatefulSet   

```
kubectl apply -f ./statefulsets
```
```bash
kubectl run -it srvlookup --image=tutum/dnsutils --rm --restart=Never -- dig SRV postgresql-db-service.grupo00.svc.cluster.local
```

```bash
;; ANSWER SECTION:
postgresql-db-service.grupo00.svc.cluster.local. 5 IN SRV 0 50 5432 postgresql-db-0.postgresql-db-service.grupo00.svc.cluster.local.
postgresql-db-service.grupo00.svc.cluster.local. 5 IN SRV 0 50 5432 postgresql-db-1.postgresql-db-service.grupo00.svc.cluster.local.

;; ADDITIONAL SECTION:
postgresql-db-0.postgresql-db-service.grupo00.svc.cluster.local. 5 IN A 10.1.148.221
postgresql-db-1.postgresql-db-service.grupo00.svc.cluster.local. 5 IN A 10.1.148.231
```

## Links
- https://kubernetes.io/es/docs/concepts/services-networking/service/#servicios-headless


# Con Helm Chart
https://github.com/bitnami/charts/tree/main/bitnami/postgresql/#installing-the-chart

## Postgres
```
helm install helm-postgres bitnami/postgresql  -f values.yaml
```
### URL Acceso
- helm-postgres.rds.svc.cluster.local

### Clave
- export POSTGRES_PASSWORD=$(kubectl get secret --namespace rds helm-postgres-postgresql -o jsonpath="{.data.postgres-password}" | base64 -d)

### port-forward
### admin
```
kubectl port-forward  svc/pgadmin-service 8080:80
```
### RDS
```
kubectl port-forward  svc/helm-postgres-postgresql 54320:5432
```


## pgadmin
https://www.enterprisedb.com/blog/how-deploy-pgadmin-kubernetes
```
kubectl apply -f pgadmin.yaml
secret/pgadmin 
configmap/pgadmin-config 
service/pgadmin-service 
statefulset.apps/pgadmin 
```

